const winston = require('winston');
const path = require('path');
const toggl = require('toggl-api');
const togglHelper = require('./togglWrapper');

class App {
    constructor(config, paths) {
        this.config = config;
        this.paths = paths;
        this.initLogger();
    }

    async init() {
        await this.initToggl();
        this.initIntervals();
    }

    /**
     * Configure Winston logger
     * @return {winston.Logger}
     */
    initLogger() {
        this.logger = winston.createLogger({
            level: 'info',
            format: winston.format.json(),
            transports: [
                new winston.transports.File({
                    filename: path.join(this.paths.logs, 'error.log'),
                    level: 'error'
                }),
                new winston.transports.File({
                    filename: path.join(this.paths.logs, this.config.app.logs.commonlog)
                })
            ]
        });
        this.logger.add(
            new winston.transports.Console({
                format: winston.format.simple()
            })
        );
    };

    /**
     * Create/Configure connection to Intervals API
     */
    initIntervals() {
    }

    /**
     * Create/Configure connection to Toggl API
     */
    async initToggl() {
        // @todo: toggl-specific props should be assigned to this.toggl.*, not this.toggl*
        this.toggl = new toggl({
            apiToken: this.config.toggl.apikey
        });
        this.togglUser = await togglHelper.getUser(this.toggl);
        this.togglUserTags = await togglHelper.loadTags(this.toggl, this.togglUser, this.config);
    }

    /**
     * Facade/Wrapper for querying the togglHelper.getDetailedReportByTag
     * @return {Promise<void>}
     */
    async loadTogglDataset() {
        let reporttags = this.togglUserTags.ready.filter(tag => {
            return tag.wid === this.config.toggl.workspace;
        });
        return await togglHelper.getDetailedReportByTag(
            this.toggl,
            this.config.toggl.workspace,
            this.togglUser.id,
            reporttags,
            this.config.toggl.reportMin
        );
    }
}

module.exports = {
    App
};