/**
 * Abstracts-away Intervals' API Endpoints for the sake of maintenance and potential portability.
 * @param apibase
 * @return {{base: *, taskfromlocal: string, taskfromglobal: string, projectfromglobal: string, time: string, me: string}}
 */
const buildIntervalsEndpoints = (apibase) => {
    return {
        base: apibase,
        taskfromlocal: apibase + '/task/?localid=',
        taskfromglobal: apibase + '/task/',
        projectfromglobal: apibase + '/project/',
        time: apibase + '/time/',
        me: apibase + '/me/',
    }
}

module.exports = {
    buildIntervalsEndpoints,
};