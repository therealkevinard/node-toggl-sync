const _ = require('underscore');

/**
 * Get logged-in user info
 * @param toggl
 * @return {Promise<*>}
 */
const getUser = async (toggl) => {
    return new Promise((resolve, reject) => {
        toggl.getUserData({}, (err, user) => {
            if (err) {
                reject(err)
            }
            resolve({
                email: user.email,
                fullname: user.fullname,
                id: user.id,
                workspaces: user.workspaces,
            });
        });
    })
};

/**
 * Loads tags logged-in user can work with. Respects multiple workspaces.
 * @todo allow workspace white- and black-listing
 * @param toggl
 * @param togglUser
 * @param appConfig
 * @return {Promise<*>}
 */
const loadTags = async (toggl, togglUser, appConfig) => {
    let promises = [];

    // respect all user workspaces
    togglUser.workspaces.forEach(ws => {
        promises = [
            ...promises,
            new Promise((resolve, reject) => {
                toggl.getWorkspaceTags(ws.id, (err, tags) => {
                    if (err) {
                        reject(err)
                    }
                    resolve(tags);
                })
            })
        ]
    });

    return new Promise((resolve, reject) => {
        Promise.all(promises)
            .then(resolves => {
                let tags = {
                    ready: [],
                    done: [],
                };
                resolves.forEach(resolve => {
                    //-- get keyed flags from app config
                    let config_readyTags = appConfig.app.toggltags_ready;
                    let config_doneTags = appConfig.app.toggltags_success;

                    //-- for intersections: get only names from resolve tags
                    let resolve_names = resolve.map(tag => {
                        return tag.name
                    });

                    //-- get intersection between ws tags and app config tags. use the intersection to filter original response's full objects
                    let x_readytags = _.intersection(resolve_names, config_readyTags);
                    let readyTagObjects = resolve.filter(tagObject => {
                        return x_readytags.indexOf(tagObject.name) > -1;
                    });
                    tags.ready = [
                        ...tags.ready,
                        ...readyTagObjects
                    ];

                    let x_donetags = _.intersection(resolve_names, config_doneTags);
                    let doneTagObjects = resolve.filter(tagObject => {
                        return x_donetags.indexOf(tagObject.name) > -1;
                    })
                    tags.done = [
                        ...tags.done,
                        ...doneTagObjects
                    ];
                });
                resolve(tags);
            })
    })
}

/**
 * Load detailed report with given params. The core query for ETL dataset
 * @todo: there's an arch. conflict here. this method only supports single workspace, but the tags' method goes to lengths to support multiple spaces. @prefer multiple spaces for now
 */
const getDetailedReportByTag = (toggl, workspaceid, userid, bytag, startDate) => {
    let tags_cs = bytag.map(tagObject => {
        return tagObject.id;
    }).join(','); // reduce array of tag objects to comma-separated list of names //@todo: there's a lot of shuffling tags between names and objects and back - potential room for optimization

    let report_options = {
        user_agent: 'node_api',
        workspace_id: workspaceid,
        tag_ids: tags_cs,
        user_ids: userid,
        since: startDate.format('YYYY-MM-DD'),
        display_hours: 'decimal',
    };

    return new Promise((resolve, reject) => {
        toggl.detailedReport(report_options, (err, response) => {
            if (err) {
                reject(err)
            }
            let maxRecs = 30; // intervals has pretty aggressive api limits. choke the responses to a length
            resolve(response.data.slice(0, maxRecs));
        })
    });
}

module.exports = {
    getUser,
    loadTags,
    getDetailedReportByTag,
};