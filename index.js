//-- 3rd-party modules
const TogglClient = require('toggl-api');
const request = require('request-promise').request;
const parse_str = require('pixl-xml');
const moment = require('moment');
const path = require('path');

//-- internal modules
const setup = require('./src/setup');
const enpoints = require('./src/endpoints');

//-- app-level references
let config = require('./secrets/config');
let paths = {
    root: __dirname,
    modules: path.join(__dirname, 'src'),
    logs: path.join(__dirname, 'logs'),
};

//--
let intervalsEndpoints = enpoints.buildIntervalsEndpoints(config.intervals.apiBase);

//--
let updatedTimers = [];

(async () => {
    let app = new setup.App(config, paths);
    await app.init();
    let time = await app.loadTogglDataset();
    debugger;
})();