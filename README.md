# Node Toggl Sync 

I work for a company the uses [Intervals](https://www.myintervals.com/). I like [Toggl](https://toggl.com/). 

This is a pretty routine ETL-is tool that allows me to log, curate, and prepare my time reports using Toggl and all its great tools, then shuffle the time into Intervals for company use. 

- Uses API connections on both ends. 
- Allows configurable Toggl tags to flag timers as ready or complete, preventing duplicate or accidental pushes
- Uses regex matching in the timer description to correctly correlate time and task (doesn't require an _official_ integration)
 
 
**Usage:**

`@todo: add basic usage`  
`@todo: add cron usage` 
