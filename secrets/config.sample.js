module.exports = {
    /**
     * Configure Toggle end of the exchange
     */
    toggl: {
        apikey: 'toggl-api-key', // toggl api key
        workspace: 1234567, // toggl workspace id
        tagids: {
            test: 1234567, // for dev: test tag id
            stage: 1234567, // toggl tag id: timers to consider for ETL
            apilogged: 1234567, // toggl tag id: timers that have been successfully logged @deprecated?
            logged: 1234567, // toggl tag id: timers that have been successfully logged @deprecated?
        }
    },
    /**
     * Configure Intervals end of the exchange
     */
    intervals: {
        apiBase: 'https://api.myintervals.com',
        apikey: 'intervals-api-key', // intervals api key
        me: 123456, // from intervals: your personal user id
        worktypes: {
            dev: 123456 // from intervals: map worktypes to intervals ids. I only do development, but you can potentially add other
        }
    },
    /**
     * App-level configuration
     */
    app: {
        logs: {
            commonlog: 'log-out.json',
            toggletimers: 'toggl.json'
        },
        append_desc: ' [api logged]', // for retrospection, this will be appended to timer descriptions pushed to intervals.
        // prefer toggltags_* over the id configs above. allows better portability across workspaces. @todo: provide workspace-specific configs
        toggltags_ready: [
            'staged-for-api-log'
        ],
        toggltags_success: [
            'api-logged-intervals'
        ],
    }
};